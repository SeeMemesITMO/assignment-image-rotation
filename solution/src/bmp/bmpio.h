#ifndef LAB_BMPIO_H
#define LAB_BMPIO_H

#include <stdint.h>
#include <stdio.h>

#include "../image/image.h"

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_STREAM_NULL,
    READ_TARGET_NULL,
    READ_MALLOC_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_STREAM_NULL,
    WRITE_SOURCE_NULL,
    WRITE_ERROR
};


enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, struct image *img);


#endif //LAB_BMPIO_H
